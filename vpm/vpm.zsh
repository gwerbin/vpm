#!/usr/bin/env zsh

# Vim Package Manager version 0.0.1 - CLI-only Vim package manager
# Copyright (C) 2019 Gregory Werbin

# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


zmodload zsh/zutil

emulate zsh
setopt                 \
	err_exit           \
	NO_unset           \
	warn_create_global \
	warn_nested_var

alias const='local -r'
alias global='typeset -g'

VERSION=0.0.1

ERR_USAGE=1
ERR_HOMEDIR=2
ERR_PACKAGEEXISTS=3
ERR_NOTIMPLEMENTED=99

# TODO: actual URI/URL scheme detection instead of just assuming Github
# TODO: -y/--yes
# TODO: accept more than 1 package to install, upgrade, remove?
# TODO: don't hardcode nvim path
# TODO: don't rely on PATH for commands like git, rm, etc?
# TODO: update/upgrade (downgrade?), specify version (or git branch/tag)
# TODO: other VCSes? Hg, Pijul?
# TODO: some kind of cache
# TODO: document #SETS comments and _function prefixes (dev docs)
# TODO: testing (pexpect?)
# TODO: fancy stuff - readme, website, etc
# TODO: standalone helptags command and other "plumbing" commands
# TODO: list currently installed
# TODO: disambiguate naming when installed from different URI/URL
# TODO: some kind of package database other than the filesystem itself? store metadata inside or alongside installed package dir?
# TODO: dependency resolution (yikes)
# TODO: error if package is not installed in vpm-upgrade


read -r -d '' VERSION_STRING <<TXT || true
Vim Package Manager v$VERSION
TXT

read -r -d '' DOC_STRING <<TXT || true
Usage:
  vpm list    [ options ]
  vpm install [ options ] [ uri | url ]
  vpm remove  [ options ] [ package ]

Arguments:
  uri                           <scheme>:<path>
    Accepted formats:
      file:/path/to/file        Shorthand for file:///path/to/file [NOT IMPLEMENTED]
      github:username/reponame  Github
	  vimorg:script_id                                             [NOT IMPLEMENTED]
      ghgist:username/reponame  Github "Gist"                      [NOT IMPLEMENTED]
      gitlab:username/reponame  Gitlab                             [NOT IMPLEMENTED]

  url                           <scheme>://<authority>/<path>
    Accepted schemes:
      file                                                         [NOT IMPLEMENTED]
        Can be a single file or a directory
      scp                                                          [NOT IMPLEMENTED]
      (s)ftp                                                       [NOT IMPLEMENTED]
      http(s)                                                      [NOT IMPLEMENTED]
      rsync                                                        [NOT IMPLEMENTED]
      git+<any of the above>                                       [NOT IMPLEMENTED]
        Uses Git to clone from URL

    A scheme with "git+" prepended will attempt to use Git, e.g.
      git+https://git.example.net/vim/myplug.git

    As per RFC 1738, if the "authority" is blank, it is interpreted as "localhost", e.g.
      file:///home/leah/projects/vim-extra-special
	is equivalent to
      file://localhost/home/leah/projects/vim-extra-special
	which will try to install the package from the current filesystem (without
	using the network)

Common options:
  -v                           Print version and exit.

  -h                           Print help and exit.

  -Q
  --quiet                        Be quiet, can be repeated up to 3 times. [NOT IMPLEMENTED]
                                   0:  Default
                                   1:  Emit only errors and warnings
                                   2:  Emit only errors
                                   3+: Complete silence (use $? for error status)
                               Error if used in conjunction with -V.

  -V
  --verbose                      Be verbose, can be repeated up to 3 times.
                                   0:  Default
                                   1:  Show more exeuction details [NOT IMPLEMENTED]
                                   2:  Same as 1, with additional environment info [NOT IMPLEMENTED]
                                   3+: Line-by-line execution trace
                               Error if used in conjunction with -Q.

Install options:
  -f
  --format                     File format when extension is unrecognized. [NOT IMPLEMENTED]
                                 uncompressed (default)
                                 vimball
                                 .zip .gz .xz .bz2
                                 .tar.gz .tar.xz .tar.bz2
                                 .tgz .txz .tbz2
                               Warning emitted if this option is given but a valid file extension was detected.
                               Error emitted if this option is given in conjunction with a Git-based scheme.

  -g <arg>                     <arg> passed to 'git clone'.

  -d
  --subdir                     Subdirectory containing the top-level package. Default is '/' if omitted.

Environment variables
  VPM_HOME                     Where packages are installed. Will be created on first run if missing.
                               Default: ${XDG_CONFIG_HOME:-~/.config}/nvim/pack/vpm
TXT


function die() {
	print -u2 -- ${@:2}
	exit "$1"
}


function die_not-implemented() {
	die $ERR_NOTIMPLEMENTED "Feature ${(q)1} not implemented, sorry :("
}


# SETS: package_type package_name package_url
function _parse-spec() {
	setopt local_options NO_warn_nested_var

	local -a uri_split=( ${(ps.:.)1} )
	local -a package_info=( )

	if (( $#uri_split == 1 )); then
		die $ERR_USAGE 'No scheme provided.'
	fi

	package_type="${uri_split[1]}"
	case "${package_type}" in
		gh|github)
			local -a package_info=( ${(ps./.)uri_split[2]} )
			if (( $#package_info != 2 )); then
				die $ERR_USAGE 'Invalid Github URI'
			fi
			package_name="${package_info[2]}"
			package_url="https://github.com/${uri_split[2]}"
			;;
		gl|gitlab)
			local -a package_info=( ${(ps./.)uri_split[2]} )
			if (( $#package_info != 2 )); then
				die $ERR_USAGE 'Invalid Gitlab URI'
			fi
			package_name="${package_info[2]}"
			package_url="https://gitlab.com/${uri_split[2]}"
			;;
		srht|sourcehut)
			local -a package_info=( ${(ps./.)uri_split[2]} )
			if (( $#package_info != 2 )); then
				die $ERR_USAGE 'Invalid Sourcehut URI'
			fi
			package_name="${package_info[2]}"
			package_url="https://git.sr.ht/~${uri_split[2]}"
			;;
		git+http|git+https|ftp|ftps|http|https)
			package_name="${uri_split[2]:t}"
			package_url="${uri_split[2]}"
		git+http*)
			package_name="${uri_split[2]:t}"
			package_url="${uri_split[2]}"
		*)
			die_not-implemented "scheme=${package_type}"
			;;
	esac
}


# SETS: package_path
function _path-to-package() {
	setopt local_options NO_warn_nested_var
	package_path="${VPM_HOME}/opt/${1}"
}


function vpm-install() {
	local package_type package_name package_url
	_parse-spec "$1"

	#local package_path="$VPM_HOME/opt/$package_name"
	local package_path
	_path-to-package "$package_name"
	if [[ -e "$package_path" ]]; then
		die $ERR_PACKAGEEXISTS "Package already installed at $package_path"
	fi

	local -a install_cmd
	if [[ "${package_type}" == 'git' ]]; then
		install_cmd=( git clone "$package_url" "$package_path" )
	else
		die_not-implemented "package_type=${package_type}"
	fi

	local -a helptags_cmd
	if [[ -d "${package_path}/doc" ]]; then
		helptags_cmd=( "$VIM" -u NONE -i NONE -V15 --cmd "helptags ${(q)package_path}/doc | q!" )
	fi

	local doit
	print -- "Install $package_type package $1"
	print -- Execute: ${=install_cmd}
	if (( $#helptags_cmd )); then
		print -- Execute: ${=helptags_cmd}
	fi
	read -q 'doit?Continue? (y/n) ' || true
	print

	if [[ $doit == 'y' ]]; then
		${=install_cmd}
		${=helptags_cmd}
	else
		print -- "No action taken."
	fi
}


function vpm-list() {
	find "${VPM_HOME}/opt" -maxdepth 1 -type d -exec basename {} \; | sed -n '2,${p;}'
}


function vpm-upgrade() {
	local package_type package_name package_url
	_parse-spec "$1"

	if [[ "$package_type" != 'git' ]]; then
		die_not-implemented "package_type=${package_type}"
	fi

	local package_path
	_path-to-package "$package_name"

	git -C "$package_path" pull --no-rebase
}


function vpm-uninstall() {
	local package_path
	_path-to-package "$1"

	local doit
	print -- "Uninstall $1"
    print -- Execute: rm -vrf "$package_path"
	read -q 'doit?Continue? (y/n) ' || true

	if [[ $doit == 'y' ]]; then
		rm -vrf "$package_path"
	else
		print -- "No action taken."
	fi
}


function parse-common-options() {
	local -a arg_help arg_version arg_verbose arg_quiet arg_format
	setopt local_options NO_warn_nested_var

	# -D: remove parse args from $@
	# -E: keep parsing args until - or -- even if unknown
	zparseopts -D -E --        \
		h=arg_help             \
		-help=arg_help         \
		v=arg_version          \
		-version=arg_version   \
		V+=arg_verbose         \
		-verbose+=arg_verbose  \
		Q+=arg_quiet           \
		-quiet+=arg_quiet      \
		f+=arg_format          \
		-format+=arg_format

	if (( $#arg_help )); then
		print -- "$DOC_STRING"
		exit 0
	fi

	if (( $#arg_version )); then
		print -- "$VERSION_STRING"
		exit 0
	fi

	if (( $#arg_quiet && $#arg_verbose )); then
		print -u2 -- 'Cannot use both -Q and -V.'
		exit $ERR_USAGE
	elif (( $#arg_quiet )); then
		common_options[verbosity]=-$#arg_quiet
	elif (( $#arg_verbose )); then
		common_options[verbosity]=$#arg_verbose
	fi

	if (( ${common_options[verbosity]} >= 3 )); then
		setopt xtrace
	elif (( ${common_options[verbosity]} > 0 )); then
		print -u2 -- "Verbose level ${common_options[verbosity]} not implemented"
		exit $ERR_USAGE
	elif (( ${common_options[verbosity]} < 0 )); then
		die_not-implemented "quiet level ${common_options[verbosity]}"
	fi
}


function _ensure-home() {
	if [[ ! -d "$VPM_HOME" ]]; then
		if [[ -e "$VPM_HOME" ]]; then
			die $ERR_HOMEDIR "Cannot use given VPM_HOME, already exists but not a directory: $VPM_HOME"
		else
			mkdir -p "$VPM_HOME"
		fi
	fi
}


function main() {
	local -A common_options
	common_options[format]=''
	common_options[verbosity]=0
	parse-common-options "$@"

	export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-~/.config}
	export VPM_HOME=${VPM_HOME:-${XDG_CONFIG_HOME}/nvim/pack/vpm}
	export VIM='nvim'
	_ensure-home

	case "${1:-}" in
		install|add)
			if [[ ${#@} -lt 2 ]]; then
				die $ERR_USAGE 'No packages specified for installation.'
			fi
			vpm-install "${@:2}"
			;;
		ls|list)
			if [[ ${#@} -gt 2 ]]; then
				die $ERR_USAGE "'vpm ls' does not accept additional arguments."
			fi
			vpm-list
			;;
		upgrade)
			if [[ ${#@} -lt 2 ]]; then
				die $ERR_USAGE 'No packages specified for upgrading.'
			fi
			vpm-upgrade "${@:2}"
			;;
		uninstall|remove)
			if [[ ${#@} -le 3 ]]; then
				die $ERR_USAGE 'No packages specified for removal.'
			fi
			vpm-uninstall "${@:2}"
			;;
		'')
			print -u2 -- "Subcommand is required."
			die $ERR_USAGE 'Invalid or unkown input. See: vpm --help.'
			;;
		*)
			print -u2 -- "Unknown subcommand: ${(q)1}"
			die $ERR_USAGE 'Invalid or unkown input. See: vpm --help.'
			;;
	esac

	#if [[ "$1" == 'install' || "$1" == 'add' ]]; then
	#	vpm-install "${@:2}"
	#elif [[ "$1" == 'remove' || "$1" == 'uninstall' ]]; then
	#	vpm-uninstall "${@:2}"
	#else
	#	print -u2 -- "Unknown command: ${(q)1}"
	#fi

	exit 0
}

main "$@"

