import
  os,
  strformat,
  strutils,
  uri

const vpmVersion = "0.0.1"
const vpmVersionDesc = &"Vim Package Manager (VPM) v{vpmVersion}"

type
  ExitStatusCode = enum
    errUsage = 1
    errHomedirUnusable = 2
    errPackageExists = 3
    errNotImplemented = 99

type
  VimPackage* = object
    name*: string
    url*: string

type
  SpecParseError* = object of Exception

# https://regex101.com/r/wh5p1z/1
const pattGithubShort = re"^([^:/]+)/([^:/])$"

proc parsePackageSpec(spec: string): VimPackage =
  # github:<username>/<reponame>[@<ref>]
  # gitlab:<username>/<reponame>[@<ref>]
  if spec.match(pattGithubShort):
    spec = &"github:{spec}"

  var uri = parseUri(spec)
  var packageUrl, packageName: string
  case uri.scheme
  of "", "file":
    packageUrl = &"file://{uri.path}"
    packageName = splitPath(uri.path).tail
  of "github":
    var pathParts = split(spec, "/")
    if len(pathParts) != 2:
      raise newException(SpecParseError, "Github URI path must have the format <username>/<password>.")
    var ghAuthor, ghRepo = pathParts
    packageUrl = &"https://github.com/{ghAuthor}/{ghRepo}"
    packageName = ghRepo
  of "gitlab":
    var pathParts = split(spec, "/")
    if len(pathParts) != 2:
      raise newException(SpecParseError, "Gitlab URI path must have the format <username>/<password>.")
    var glAuthor, glRepo = pathParts
    packageUrl = &"https://gitlab.com/{glAuthor}/{glRepo}"
    packageName = glRepo
  of "git+http", "git+https":
    packageUrl = spec
    packageName = splitPath(uri.path).tail

  return Package(name: packageName, url: packageUrl)

proc die(status_code: ExitStatusCode, message: string) =
  stderr.writeLine(message)
  quit(status_code.ord())

proc dieNotImplemented(feature: string) =
  die(errNotImplemented, &"Feature {feature} not implemented, sorry :(")

when isMainModule:
  # import cligen
  # dispatchMulti([
  #   [vpm_list],
  #   [vpm_install],
  #   [vpm_uninstall],
  #   [vpm_upgrade],
  # ])
  echo(vpmVersionDesc)
  dieNotImplemented("Hello")
