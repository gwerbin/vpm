# Package

version       = "0.1.0"
author        = "No Man's Slave"
description   = "Vim Package Manager"
license       = "GPL-3.0"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["vpm"]


# Dependencies

requires "nim >= 1.2.6"
